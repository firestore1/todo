import Vue from 'vue'
import App from './App.vue'
import db from './db'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  created() {
    db
  },

  vuetify,
  render: h => h(App)
}).$mount('#app')
