import firebase from 'firebase/app'
import 'firebase/firestore'

// Get a Firestore instance
export const db = firebase
  .initializeApp({
    apiKey: "AIzaSyCKW8AdqtT-dljieRqkzYZl9tiQj_sinrM",
    authDomain: "rayo-test.firebaseapp.com",
    databaseURL: "https://rayo-test.firebaseio.com",
    projectId: "rayo-test",
    storageBucket: "rayo-test.appspot.com",
    messagingSenderId: "599190099240",
    appId: "1:599190099240:web:216658d80b9cba68ea6c1d",
    measurementId: "G-YK29XLB76N"
  })
  .firestore()

// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
// const { Timestamp, GeoPoint } = firebase.firestore
// export { Timestamp, GeoPoint }

// if using Firebase JS SDK < 5.8.0
// db.settings({ timestampsInSnapshots: true })